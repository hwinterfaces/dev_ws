import { boolean, number, text, withKnobs } from '@storybook/addon-knobs';
import { button_standard } from './button_standard.component';
import markdown from './button_standard.docu.md';
import { action } from '@storybook/addon-actions';

export default {
  title: 'Button_Standard',
  component: button_standard,
  decorators: [withKnobs]
};

export const Initial = () => ({
  component: button_standard,
  props: {
    title:  text('Text', 'This is a button'),
    disabled: boolean('Disabled', false),
    onClick: action('onClick')
  }
});

export const Disabled = () => ({
  component: button_standard,
  props: {
    title:  text('Text', 'This is a button'),
    disabled: true,
    onClick: action('onClick')
  }
});

Initial.story = {
  parameters: {
    notes: { markdown }
  }
};
