import { Component } from '@angular/core';

@Component({
  selector: 'button_standard',
  template: `
    <button type="button" class="button_standard" [disabled]="disabled ? 'disabled': null" (click)="onClick()">
      {{title}}
    </button>
  `,
  styleUrls: ['./button_standard.styles.css']
})

export class button_standard {
  title: string;
  disabled: boolean;
  onClick(): void {
  }
}
