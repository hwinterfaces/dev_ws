import {Component} from '@angular/core';

@Component({
  selector: 'description',
  template: `
    <div class="description" (mouseover)="onMouseOver()">
      <div *ngIf="!hidden" class="Lorem-ipsum-dolor-si">{{text}}</div>
    </div>
  `,
  styleUrls: ['./description.styles.css']
})

export class description {
  text: string;
  hidden: boolean;

  onMouseOver() {

  }
}
