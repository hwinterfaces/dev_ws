import { boolean, number, text, withKnobs } from '@storybook/addon-knobs';
import { description } from './description.component';
import markdown from './description.docu.md';
import { action } from '@storybook/addon-actions';

export default {
  title: 'description',
  component: description,
  decorators: [withKnobs]
};

export const Initial = () => ({
  component: description,
  props: {
    text:  text('Text', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor'),
    hidden: boolean("Hidden",false),
    onMouseOver: action('mouseover')
  }
});

Initial.story = {
  parameters: {
    notes: { markdown }
  }
};
