import { boolean, number, text, withKnobs } from '@storybook/addon-knobs';
import { my_table_cell } from './my_table_cell.component';
import markdown from './my_table_cell.doku.md';
import { action } from '@storybook/addon-actions';

export default {
  title: 'My table Cell',
  component: my_table_cell,
  decorators: [withKnobs],
};

export const Initial = () => ({
  component: my_table_cell,
  props: {
    content:  text('Text', 'This is a Table Cell Text'),
    onMouseOver: action('onMouseOver')
  }
});

Initial.story = {
  parameters: {
    notes: { markdown }
  }
};