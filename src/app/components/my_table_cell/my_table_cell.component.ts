import { Component } from '@angular/core';

@Component({
  selector: 'my_table_cell',
  template: `
    <td class="my_table_cell" (mouseover)="onMouseOver()"> {{content}} </td>
  `,
  styleUrls: ['./my_table_cell.styles.css']
})

export class my_table_cell {
  content: string;

  onMouseOver() {
    console.log("Mouse over");
  }
}