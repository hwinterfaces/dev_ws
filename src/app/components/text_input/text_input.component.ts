import { Component } from '@angular/core';

@Component({
  selector: 'text_input',
  template: `
      <input class="text_input" type="text" (focusout)="focusout()">
  `,
  styleUrls: ['./text_input.styles.css']
})

export class text_input {
  title: string;
  hidden: boolean;
  focusout(): void {}
}


/*<div class="text_input_container">
      <input class="text_input" type="text" (focusout)="focusout()">
    </div>*/
