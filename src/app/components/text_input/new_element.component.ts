import { Component } from '@angular/core';

@Component({
  selector: 'new_element',
  template: `
    <div class="new_element">
      <h1 *ngIf="!hidden">{{title}}</h1>
    </div>
  `,
  styleUrls: ['./new_element.styles.css']
})

export class new_element {
  title: string;
  hidden: boolean;
}