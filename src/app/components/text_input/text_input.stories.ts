import { boolean, number, text, withKnobs } from '@storybook/addon-knobs';
import { text_input } from './text_input.component';
import markdown from './text_input.docu.md';
import { action } from '@storybook/addon-actions';

export default {
  title: 'Text_input',
  component: text_input,
  decorators: [withKnobs]
};

export const Initial = () => ({
  component: text_input,
  props: {
    title:  text('Text', 'This is a Text_Input'),
    focusout: action('focusout')
  }
});

Initial.story = {
  parameters: {
    notes: { markdown }
  }
};
