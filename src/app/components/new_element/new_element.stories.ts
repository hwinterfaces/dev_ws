import { boolean, number, text, withKnobs } from '@storybook/addon-knobs';
import { new_element } from './new_element.component';
import markdown from './new_element.docu.md';
import { action } from '@storybook/addon-actions';

export default {
  title: 'New_Element',
  component: new_element,
  decorators: [withKnobs]
};

export const Initial = () => ({
  component: new_element,
  props: {
    title:  text('Text', 'This is a New_Element'),
    hidden: boolean("Hidden",false)
  }
});

Initial.story = {
  parameters: {
    notes: { markdown }
  }
};