import { Component } from '@angular/core';

@Component({
  selector: 'app-button-small',
  template: `
    <button *ngIf="!hidden" (click)="onClick()">
      <span>{{ title }}</span>
    </button>
  `,
  styleUrls: ['./button-small.styles.css']
})
export class ButtonSmallComponent {
  title: string;
  hidden: boolean;

  onClick(): void {
    console.log('Small button was clicked!');
  }
}
