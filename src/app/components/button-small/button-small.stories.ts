import { boolean, number, text, withKnobs } from '@storybook/addon-knobs';
import markdown from './button-small.docu.md';
import { ButtonSmallComponent } from './button-small.component';
import { action } from '@storybook/addon-actions';

export default {
  title: 'button_small',
  component: ButtonSmallComponent,
  decorators: [withKnobs]
};

export const Initial = () => ({
  component: ButtonSmallComponent,
  props: {
    title:  text('Text', 'Lalala'),
    hidden: boolean('Hidden', false),
    onClick: action('onClick')
  }
});

Initial.story = {
  parameters: {
    notes: { markdown }
  }
};
